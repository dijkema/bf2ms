#!/bin/env python3

from argparse import ArgumentParser
import logging
import h5py
from dataclasses import dataclass
import numpy as np
from casacore.tables import default_ms, makearrcoldesc, maketabdesc, table as MSTable

logging.basicConfig(level=logging.DEBUG, format='%(name)s - %(levelname)s %(asctime)s :  %(message)s')
logger = logging.getLogger('bf2ms')


def parse_arguments():
    parser = ArgumentParser(description='Convert beamformed hdf5 into measurement sets')
    parser.add_argument('bf_path', help='input beamformed file path')
    parser.add_argument('subband', help='subband to extract', type=int)
    parser.add_argument('--output_ms', help='output file path', default="./OUT.MS")
    parser.add_argument('--beam_tab', help='select the beam tab', default='BEAM_000')

    return parser.parse_args()



@dataclass
class BFMetadata:
    antenna_set: str = None
    station: str = None
    observation_frequency_min: float = None

    channel_width: float = None
    channel_per_subband: int = None
    subband_width: float = None

    frequency_min: float = None
    frequency_max: float = None

    channel_width_unit: float = 'HZ'
    frequency_unit: str = 'Hz'

@dataclass
class SpectralTableRow:
    ref_frequency: float
    name: str
    num_chan: int
    total_bandwidth: float
    chan_freq: list
    chan_width: list
    effective_bw: list
    net_sideband: int = 0
    if_conv_chain: int = 0
    freq_group_name: str = ''
    freq_group: int = 0
    flag_row: bool = False
    meas_freq_ref: int = 5

    @staticmethod
    def create(subband_number, start_frequency, end_frequency, n_channels):
        name = 'SB-{}'.format(subband_number)
        num_chan = n_channels

        total_bandwidth = end_frequency - start_frequency
        chan_width = total_bandwidth / n_channels
        ref_frequency = (end_frequency + start_frequency) / 2.
        chan_freq = np.linspace(start_frequency, end_frequency, n_channels) + chan_width / 2.
        chan_width = np.zeros_like(chan_freq) + chan_width

        return SpectralTableRow(ref_frequency=ref_frequency,
                                name=name,
                                num_chan=num_chan,
                                total_bandwidth=total_bandwidth,
                                chan_freq=chan_freq,
                                chan_width=chan_width,
                                effective_bw=chan_width)

    def add_into_spectral_window_table(self, spec_window_table: MSTable):
        spec_window_table.addrows(1)
        nrows = spec_window_table.nrows()
        for att_name, value in self.__dict__.items():
            spec_window_table.putcell(att_name.upper(), nrows - 1, value)


def read_hdf5_file(file_path, beam_tab_name: str, subband_number: int):
    file_in = h5py.File(file_path, 'r')
    meta = BFMetadata()
    meta.antenna_set = file_in.attrs['ANTENNA_SET']
    stations = file_in.attrs['OBSERVATION_STATIONS_LIST']
    assert(len(stations) == 1)
    meta.station = stations[0].decode()
    meta.observation_frequency_min = file_in.attrs['OBSERVATION_FREQUENCY_MIN']

    beam_tab = file_in['/SUB_ARRAY_POINTING_000'][beam_tab_name]
    meta.channel_width = beam_tab.attrs['CHANNEL_WIDTH']
    meta.channel_per_subband = beam_tab.attrs['CHANNELS_PER_SUBBAND']
    meta.subband_width = beam_tab.attrs['SUBBAND_WIDTH']
    meta.frequency_min = meta.observation_frequency_min + meta.subband_width * subband_number
    meta.frequency_max = meta.frequency_min + meta.subband_width

    return meta


def fill_spectral_window(ms_path, meta: BFMetadata, subband_number):
    spectral_window_table = MSTable("/".join((ms_path, 'SPECTRAL_WINDOW')), readonly=False)
    spec_table = SpectralTableRow.create(subband_number,
                                         meta.frequency_min,
                                         meta.frequency_max,
                                         meta.channel_per_subband)
    spec_table.add_into_spectral_window_table(spectral_window_table)



def create_default_ms(file_path, n_channels, n_polarization):
    logging.debug('creating empty ms')
    data_col_description = makearrcoldesc('DATA', 0., ndim=2, shape=[n_channels, n_polarization])
    ms = default_ms(file_path, tabdesc=maketabdesc((data_col_description)))
    return ms


def main():
    arguments = parse_arguments()
    logger.debug('reading bf file meta')
    h5_beam_meta = read_hdf5_file(arguments.bf_path, arguments.beam_tab, arguments.subband)
    logger.debug('bf head content %s', repr(h5_beam_meta))
    ms_out = create_default_ms(arguments.output_ms, h5_beam_meta.channel_per_subband, 4)
    logger.debug('created ms at %s', arguments.output_ms)
    fill_spectral_window(arguments.output_ms, h5_beam_meta, arguments.subband)
    logger.debug('table filled for %s', arguments.output_ms)


if __name__ == '__main__':
    main()
